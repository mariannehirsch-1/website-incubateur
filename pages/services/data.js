export const SCALE_FILTER = ['Communes', 'Départements', 'Régions', 'EPCI'];

export const THEME_FILTER = [
  'Transition écologique',
  'Équipements et patrimoine',
  'Développement économique local',
  'Solidarités et inclusion numérique',
  'Mobilités',
  'Outils des élus et des agents',
  'Relation et communication avec les usagers',
  'Education et Enfance',
  "Accès à l'emploi",
  'Données publiques',
  'Dématérialisation',
  'Finances publiques',
  'Démocratie',
  'Gestion des risques',
  'Gestion des interventions',
  'Déchets',
  'Cartographie',
  'Assemblée délibérante à distance',
  'Bureautique',
  'Sobriété numérique',
  'Site web',
  'Commande publique',
  'Signature éléctronique',
  'Faciliter les démarches des habitants',
  'Simulateur',
].sort();

export const PROGRAM_OPTIONS = [
  "Startup d'Etat ou de territoires",
  'Service lauréat FR',
  'Tous les programmes',
];
