const API_URL = process.env.API_URL || 'http://localhost:8080';
const MEILISEARCH_HOST =
  process.env.MEILISEARCH_HOST || 'http://localhost:7700';
const { API_TOKEN, MEILISEARCH_API_KEY, MATOMO_URL, MATOMO_SITE_ID } =
  process.env;

export default {
  telemetry: false,

  head: {
    title: 'Incubateur des Territoires',
    htmlAttrs: {
      lang: 'fr',
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
      { name: 'format-detection', content: 'telephone=no' },
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/images/favicon.ico' }],
    script: [
      {
        src: 'https://incubateur-territoires.github.io/feedback-widget/widget.js',
      },
      {
        async: true,
        defer: true,
        src: '/scripts/focus-visible.min.js',
      },
    ],
  },

  css: ['~/assets/style/main.css'],

  plugins: [{ src: '~/plugins/http.js' }, { src: '~/plugins/meilisearch.js' }],

  buildModules: ['@nuxt/typescript-build'],

  modules: [
    '@nuxtjs/axios',
    ['nuxt-matomo', { matomoUrl: MATOMO_URL, siteId: MATOMO_SITE_ID }],
    'nuxt-logrocket',
  ],

  publicRuntimeConfig: {
    apiURL: API_URL,
    apiToken: API_TOKEN,
    meilisearchHost: MEILISEARCH_HOST,
    meilisearchApiKey: MEILISEARCH_API_KEY,
    mobileBreakpoint: 992,
  },

  logRocket: {
    logRocketId: 'xz8jmz/incubateuranctgouvfr',
  },
};
