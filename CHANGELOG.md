# CHANGELOG

## 1.14.1
### FIX
- Contact button typo
- Condition to display external service link

## 1.14.0
### FEATURES
- Add download links
### FIX
- Update logos
- Display logic for job offers

## 1.12.3 (2022-10-03)
### FIX
- Services tracking
- Do not display pagination if not needed

## 1.12.2 (2022-09-22)
### FIX
- Disable fetching unpublished ressources

## 1.12.1 (2022-09-21)
### FIX
- Logo icons
- Main logo

## 1.12.0 (2022-09-20)
### FEATURES
- Update logos

## 1.11.1 (2022-09-20)
### FIX
- Crop service image
- Set contact buttons as primary
- Minor margin fixes

## 1.11.0 (2022-09-19)
### FEATURES
- Events displayed on home
- Full screen services page
- Contact section for service page
- UI updates (search bar, cards, etc)
- Improve tracking
- Add images to service page
### FIX
- Conditional tracking

## 1.10.0 (2022-08-30)
### FEATURES
- Add tab navigation to services page
### FIX
- UI fixes
- Use DSFR icons for external links

## 1.9.0 (2022-08-18)
### Features
- Bump DSFR to 1.7.2
### FIX
- Tracking setup
- Linting
- Accessibility
- Revert "Redirect and display error if service is in draft"

## 1.8.1 (2022-08-02)
### FIX
- HTML accessibility related fixes

## 1.8.0 (2022-08-01)
### FEATURES
- Redirect and display error if service is in draft
### FIX
- Matomo tracking
- Contact email field for services
- Semantic HTML tags

## 1.7.0 (2022-07-26)
### FEATURES
- Improve semantic in HTML tags
- Add skip links
- Focus visible on keyboard navigation
- Remove tag in header menu
- Add accessibility lint rules
- Add update date in service details page
### FIX
- Accessibility for emojis
- Add linter in CI flow
- Navigation links match slug routes
- Tracking is conditional

## 1.6.1 (2022-07-20)
### FIX
- Set service description as vhtml
- Update services page to fit updated database fields

## 1.6.0 (2022-07-18)
### FEATURES
- Modal component
- Service page responsive UI
- Custom highlight button
- Header submenu active UI
- Add 'in test' tag to service details
- Add icons to offers dropdown
- Add search value to URL
### FIX
- Matomo status check

## 1.5.0 (2022-07-05)
### FEATURES
- Track 404 origin page
### FIX
- Remove contact section in service details page
- Typos

## 1.4.0 (2022-06-27)
### FEATURES
- "Accompagnement sur mesure" page
- "Incubateurs locaux" page
- Add links to new pages
- Dropdown menu for offer pages
- Emojis to prefix test on home page cards
### FIX
- Publication date for job offers
- Typos wording and ui fixes

## 1.3.0 (2022-05-13) 
### FEATURES
- 404 custom page
### FIX
- Mobile video display for about page
- Feedback-widget position
- Mobile offer page title
- Redirect incorrect slugs to their parent

## 1.2.2 (2022-05-11)
### FIX
- Mobile header navigation
- Mobile feedback-widget

## 1.2.1 (2022-05-10)
### FIX
- Pagination previous and next navigation
- Forum link in offer page

## 1.2.0 (2022-05-09)
### FEATURES
- Page "details de services"
- Tracking on search
- Search bar on landing page

### FIX
- Search input
- UI hotfixes
- Refactor card component

## 1.1.1 (2022-04-26)
### FEATURES
- Page "services" provide sharable filter with query param
- Track contact button click on page "service"
- Replace under construction by a link button on page "offres" 
### FIX
- Typos
- Filter behavior
- Conflicts between SSR and client rendering
- Remove JS DSFR
- Special item in nav z-index
- Card links

## 1.1.0 (2022-04-19)
### FEATURES
- Page "a propos"
- Page "services"
- Meilisearch plugin
### FIX
- Contact form
- Remove subtitle in offer page
- UI fix on link button

## 1.0.5 (2022-03-28)
### FEATURES
- Matomo setup
- LogRocket setup

## 1.0.4 (2022-03-25)
### FEATURES
- Pages "actualites"
- Created Vue components to embed dsfr

## 1.0.3 (2022-03-16)
### FEATURES
- Authenticated api connection

## 1.0.2 (2022-03-15)
### FEATURES
- Pages "investigation"

## 1.0.1 (2022-03-14)
### FIX
- Favicon updated

## 1.0.0 (2022-03-11)
### FEATURES
- Page home
- Page "mentions legales"
- Pages "recrutement"
- Page "contact"
- Page "donnees personnelles"
- Page "offre"
- Plan du site
- Connection with backend api
- General nuxt setup
